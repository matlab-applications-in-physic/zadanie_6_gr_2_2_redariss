%Author: Radoslaw Paluch
%Institute of Physics
%Technical Physics

%to operate program must be downloaded with script feelinglike.m
%and put in the same directory

clear;

%adding path to jsonlab in working directory
addpath('jsonlab');


%creating cell array of locations of stations

stations ={'katowice','raciborz','krakow'};


fprintf('weather.m started\n');

%checking if the file exists, if so looking for max/min values
if(exist('weatherData.csv')~=0)
%opening and reading data from file
  fwd=fopen('weatherData.csv','r');
  dataSet=textscan(fwd,'%s %s %s %f %f %f %f %f','Delimiter',';','HeaderLines',1);
  fclose(fwd);
 %determining max and min with their indeks
    for n=1:5
      [maks(n),indeksmaks(n)]=max(dataSet{n+3});
      [minim(n),indeksmin(n)]=min(dataSet{n+3});
    end
    %displaying results
    fprintf('highest/lowest registered temperature: %.2f/%.2f [C] at %s/%s on %s/%s \n\n',maks(1),minim(1),...
    cell2mat(dataSet{3}(indeksmaks(1))),cell2mat(dataSet{3}(indeksmin(1))),cell2mat(dataSet{1}(indeksmaks(1))),...
    cell2mat(dataSet{1}(indeksmin(1))));
    
    fprintf('highest/lowest calculated sensed temperature: %.2f/%.2f [C] at %s/%s on %s/%s\n\n',maks(2),minim(2),...
    cell2mat(dataSet{3}(indeksmaks(2))),cell2mat(dataSet{3}(indeksmin(2))),cell2mat(dataSet{1}(indeksmaks(2))),...
    cell2mat(dataSet{1}(indeksmin(2))));
    
    fprintf('highest/lowest registered pressure: %.2f/%.2f [hPa] at %s/%s on %s/%s\n\n',maks(3),minim(3),...
    cell2mat(dataSet{3}(indeksmaks(3))),cell2mat(dataSet{3}(indeksmin(3))),cell2mat(dataSet{1}(indeksmaks(3))),...
    cell2mat(dataSet{1}(indeksmin(3))));
    
    fprintf('highest/lowest registered humidity: %.2f/%.2f [o|o] at %s/%s on %s/%s\n\n',maks(4),minim(4),...
    cell2mat(dataSet{3}(indeksmaks(4))),cell2mat(dataSet{3}(indeksmin(4))),cell2mat(dataSet{1}(indeksmaks(4))),...
    cell2mat(dataSet{1}(indeksmin(4))));
    
    fprintf('highest/lowest so far registered wind speed: %.2f/%.2f [km/h] at %s/%s on %s/%s\n\n',maks(5),minim(5),...
    cell2mat(dataSet{3}(indeksmaks(5))),cell2mat(dataSet{3}(indeksmin(5))),cell2mat(dataSet{1}(indeksmaks(5))),...
    cell2mat(dataSet{1}(indeksmin(5))));

 
end
%main loop working for unspecified time
while(1==1)
  %obtaining time data from PC
  time=clock;
  minutes=time(5);
  %checking if minutes value is correct, if so downloading data
  if(minutes==45)
    fprintf('Loading...\n')
    %downloading and decoding JSON from URLs of given stations
    for k=1:size(stations,2)
      weatherData1=urlread(cell2mat(['https://wttr.in/' stations(k) '?format=j1']));
      weatherData2=urlread(cell2mat(['https://danepubliczne.imgw.pl/api/data/synop/station/' stations(k) '/format/json']));
      imgwStation(k)=loadjson(weatherData2);
      wttrStation(k)=loadjson(weatherData1);
    end
    
    
    %putting obtained data into specified variables
    for l=1:size(stations,2)
      humidityWtt(l) = str2double(wttrStation(l).current_condition{1,1}.humidity);
      probingTimeWtt{l} =datestr(wttrStation(l).current_condition{1,1}.observation_time,'HH:MM');
      pressureWtt(l) = str2double(wttrStation(l).current_condition{1,1}.pressure);
      tempWtt(l)=str2double(wttrStation(l).current_condition{1,1}.temp_C);
      windWtt(l) = str2double(wttrStation(l).current_condition{1,1}.windspeedKmph);
      dateWtt{l} = datestr(wttrStation(l).weather{1}.date,29);
      feelTempWtt(l) = str2double(wttrStation(l).current_condition{1,1}.FeelsLikeC);
    end
    
    for m=1:size(stations,2)
      dateImgw{m} = imgwStation(m).data_pomiaru;
      probingTimeImgw{m} = [num2str(imgwStation(m).godzina_pomiaru),':00'];
      tempImgw(m)=str2double(imgwStation(m).temperatura);
      windImgw(m) = str2double(imgwStation(m).predkosc_wiatru);
      humidityImgw(m) = str2double(imgwStation(m).wilgotnosc_wzgledna);
      pressureImgw(m)=str2double(imgwStation(m).cisnienie);
      % external function calculating sensed temperatrue
      feelTempImgw(m)=feelinglike(tempImgw(m),windImgw(m),humidityImgw(m));
             
            
    end
    %opening file to adding 
    fwd=fopen('weatherData.csv','a');
    %checking if the file is empty, if so putting header lines
    logic=dir('weatherData.csv');
    if(logic.bytes==0)
    title1 = {'Day','Station','Time','Temperature','Sensed_temperature','Pressure','Humidity',...
    'Wind_speed'};
    fprintf(fwd,'%s;%s;%s;%s;%s;%s;%s;%s\n',title1{:});
    end
    %writing data from matlab to the file
    for n=1:size(stations,2)
      fprintf(fwd,'%s;%s;%s;%f;%f;%f;%f;%f\n',dateImgw{n},stations{n},probingTimeImgw{n},...
      tempImgw(n),feelTempImgw(n),pressureImgw(n),humidityImgw(n),windImgw(n));
    end
    for o=1:size(stations,2)
      fprintf(fwd,'%s;%s;%s;%f;%f;%f;%f;%f\n',dateImgw{o},stations{o},probingTimeWtt{o},...
      tempWtt(o),feelTempWtt(o),pressureWtt(o),humidityWtt(o),windWtt(o));
    end
    fclose(fwd);
    %opening file for reading
    fwd=fopen('weatherData.csv','r');
    dataSet=textscan(fwd,'%s %s %s %f %f %f %f %f','Delimiter',';','HeaderLines',1);
    fclose(fwd);
    %looking for max/min
    for n=1:5
      [maks(n),indeksmaks(n)]=max(dataSet{n+3});
      [minim(n),indeksmin(n)]=min(dataSet{n+3});
    end     
    
    
    fprintf('Done!\n')
    %displaying so far registered max/min values
    fprintf('highest/lowest registered temperature: %.2f/%.2f [C] at %s/%s on %s/%s \n\n',maks(1),minim(1),...
    cell2mat(dataSet{3}(indeksmaks(1))),cell2mat(dataSet{3}(indeksmin(1))),cell2mat(dataSet{1}(indeksmaks(1))),...
    cell2mat(dataSet{1}(indeksmin(1))));
    
    fprintf('highest/lowest calculated sensed temperature: %.2f/%.2f [C] at %s/%s on %s/%s\n\n',maks(2),minim(2),...
    cell2mat(dataSet{3}(indeksmaks(2))),cell2mat(dataSet{3}(indeksmin(2))),cell2mat(dataSet{1}(indeksmaks(2))),...
    cell2mat(dataSet{1}(indeksmin(2))));
    
    fprintf('highest/lowest registered pressure: %.2f/%.2f [hPa] at %s/%s on %s/%s\n\n',maks(3),minim(3),...
    cell2mat(dataSet{3}(indeksmaks(3))),cell2mat(dataSet{3}(indeksmin(3))),cell2mat(dataSet{1}(indeksmaks(3))),...
    cell2mat(dataSet{1}(indeksmin(3))));
    
    fprintf('highest/lowest registered humidity: %.2f/%.2f [o|o] at %s/%s on %s/%s\n\n',maks(4),minim(4),...
    cell2mat(dataSet{3}(indeksmaks(4))),cell2mat(dataSet{3}(indeksmin(4))),cell2mat(dataSet{1}(indeksmaks(4))),...
    cell2mat(dataSet{1}(indeksmin(4))));
    
    fprintf('highest/lowest registered wind speed: %.2f/%.2f [km/h] at %s/%s on %s/%s\n\n',maks(5),minim(5),...
    cell2mat(dataSet{3}(indeksmaks(5))),cell2mat(dataSet{3}(indeksmin(5))),cell2mat(dataSet{1}(indeksmaks(5))),...
    cell2mat(dataSet{1}(indeksmin(5))));
  
    %pause to download data only one time on a right minute
    %solution by Rafal Osadnik
    pause(60)
    
    
  end
end