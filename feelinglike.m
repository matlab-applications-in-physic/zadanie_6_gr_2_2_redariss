function retval=feelinglike(T,W,H)
  
  
  tF=1.8*T+32.;
  
  wMph=W/1.61;
  
  
  %windchill first
  if(tF<=50 && wMph>=3)
    feelsLike=35.74+(0.6215*tF)-35.75*(wMph^0.16)+((0.4275*tF)*(wMph^0.16));
  else
    feelsLike=tF;
  end
  
  %if not windchill, use heat index
  
  if (feelsLike==tF && tF>=80)
    feelsLike=0.5*(tF+61.0+((tF-68.0)*1.2)+(H*0.094));
    
    if (feelsLike>=80)
      feelsLike = -42.379 + 2.04901523*tF + 10.14333127*H -0.22475541*tF*H -0.00683783*tF*tF -...
      0.05481717*H*H + 0.00122874*tF*tF*H +0.00085282*tF*H*H - 0.00000199*tF*tF*H*H;
    
      if (H<13 && tF>=80 && tF<=112)
        feelsLike=feelsLike-((13-H)/4)*sqrt((17-abs(tF-95.))/17);
  
        if (H> 85 && tF>= 80 && tF <= 87)
          feelsLike = feelsLike + ((H-85)/10) * ((87-tF)/5);
        end
      end
    end     
  end 
  
  
  
  
  
retval=(feelsLike-32)/1.8;

end