%Author: Radoslaw Paluch
%Institute of Physics
%Technical Physics

%Octave 5.1.0.0

%adding path to jsonlab in working directory


clear();
addpath('jsonlab');



stations ={'katowice','raciborz','krakow'};


function retval=feelinglike(T,W,H)
  
  
  tF=1.8*T+32.;
  
  wMph=W/1.61;
  
  
  %windchill first
  if(tF<=50 && wMph>=3)
    feelsLike=35.74+(0.6215*tF)-35.75*(wMph**0.16)+((0.4275*tF)*(wMph**0.16));
  else
    feelsLike=tF;
  end
  
  %if not windchill, use heat index
  
  if (feelsLike==tF && tF>=80)
    feelsLike=0.5*(tF+61.0+((tF-68.0)*1.2)+(H*0.094));
    
    if (feelsLike>=80)
      feelsLike = -42.379 + 2.04901523*tF + 10.14333127*H -0.22475541*tF*H -0.00683783*tF*tF -...
      0.05481717*H*H + 0.00122874*tF*tF*H +0.00085282*tF*H*H - 0.00000199*tF*tF*H*H;
    
      if (H<13 && tF>=80 && tF<=112)
        feelsLike=feelsLike-((13-H)/4)*sqrt((17-abs(tF-95.))/17);
  
        if (H> 85 && tF>= 80 && tF <= 87)
          feelsLike = feelsLike + ((H-85)/10) * ((87-tF)/5);
        end
      end
    end     
  end 
  
  
  
  
  
retval=(feelsLike-32)/1.8;

end







if(isfile('weatherData.csv')==1)
  fwd=fopen('weatherData.csv','r');
  dataSet=textscan(fwd,'%s %s %s %f %f %f %f %f','Delimiter',';','HeaderLines',1);
  fclose(fwd);
    for n=1:5
      [maks(n),indeksmaks(n)]=max(dataSet{n+3});
      [minim(n),indeksmin(n)]=min(dataSet{n+3});
    end
    printf("highest/lowest registered temperature: %.2f/%.2f [C] at %s/%s on %s/%s \n\n",maks(1),minim(1),...
    cell2mat(dataSet{3}(indeksmaks(1))),cell2mat(dataSet{3}(indeksmin(1))),cell2mat(dataSet{1}(indeksmaks(1))),...
    cell2mat(dataSet{1}(indeksmin(1))));
    
    printf("highest/lowest calculated sensed temperature: %.2f/%.2f [C] at %s/%s on %s/%s\n\n",maks(2),minim(2),...
    cell2mat(dataSet{3}(indeksmaks(2))),cell2mat(dataSet{3}(indeksmin(2))),cell2mat(dataSet{1}(indeksmaks(2))),...
    cell2mat(dataSet{1}(indeksmin(2))));
    
    printf("highest/lowest registered pressure: %.2f/%.2f [hPa] at %s/%s on %s/%s\n\n",maks(3),minim(3),...
    cell2mat(dataSet{3}(indeksmaks(3))),cell2mat(dataSet{3}(indeksmin(3))),cell2mat(dataSet{1}(indeksmaks(3))),...
    cell2mat(dataSet{1}(indeksmin(3))));
    
    printf("highest/lowest registered humidity: %.2f/%.2f [o|o] at %s/%s on %s/%s\n\n",maks(4),minim(4),...
    cell2mat(dataSet{3}(indeksmaks(4))),cell2mat(dataSet{3}(indeksmin(4))),cell2mat(dataSet{1}(indeksmaks(4))),...
    cell2mat(dataSet{1}(indeksmin(4))));
    
    printf("highest/lowest so far registered wind speed: %.2f/%.2f [km/h] at %s/%s on %s/%s\n\n",maks(5),minim(5),...
    cell2mat(dataSet{3}(indeksmaks(5))),cell2mat(dataSet{3}(indeksmin(5))),cell2mat(dataSet{1}(indeksmaks(5))),...
    cell2mat(dataSet{1}(indeksmin(5))));
else

  continue
 
end
while(1==1)
  time=clock;
  minutes=time(5);
  if(minutes==45)
    printf("Loading...\n")
    
    for k=1:size(stations)(2)
      weatherData1=urlread(cell2mat(['https://wttr.in/' stations(k) '?format=j1']));
      weatherData2=urlread(cell2mat(['https://danepubliczne.imgw.pl/api/data/synop/station/' stations(k) '/format/json']));
      imgwStation(k)=loadjson(weatherData2);
      wttrStation(k)=loadjson(weatherData1);
    end
    
    
    
    for l=1:size(stations)(2)
      humidityWtt(l) = str2double(wttrStation(l).current_condition{1,1}.humidity);
      probingTimeWtt{l} =datestr(wttrStation(l).current_condition{1,1}.observation_time,'HH:MM');
      pressureWtt(l) = str2double(wttrStation(l).current_condition{1,1}.pressure);
      tempWtt(l)=str2double(wttrStation(l).current_condition{1,1}.temp_C);
      windWtt(l) = str2double(wttrStation(l).current_condition{1,1}.windspeedKmph);
      dateWtt{l} = datestr(wttrStation(l).weather{1}.date,29);
      feelTempWtt(l) = str2double(wttrStation(l).current_condition{1,1}.FeelsLikeC);
    end
    
    for m=1:size(stations)(2)
      dateImgw{m} = imgwStation(m).data_pomiaru;
      probingTimeImgw{m} = [num2str(imgwStation(m).godzina_pomiaru),':00'];
      tempImgw(m)=str2double(imgwStation(m).temperatura);
      windImgw(m) = str2double(imgwStation(m).predkosc_wiatru);
      humidityImgw(m) = str2double(imgwStation(m).wilgotnosc_wzgledna);
      pressureImgw(m)=str2double(imgwStation(m).cisnienie);
      
      feelTempImgw(m)=feelinglike(tempImgw(m),windImgw(m),humidityImgw(m));
             
            
    end
    fwd=fopen('weatherData.csv','a');
    if(dir('weatherData.csv').bytes==0)
    title1 = {'Day','Station','Time','Temperature','Sensed temperature','Pressure','Humidity',...
    'Wind speed'};
    fprintf(fwd,'%s;%s;%s;%s;%s;%s;%s;%s\n',title1{});
    end
    
    for n=1:size(stations)(2)
      fprintf(fwd,'%s;%s;%s;%f;%f;%f;%f;%f\n',dateImgw{n},stations{n},probingTimeImgw{n},...
      tempImgw(n),feelTempImgw(n),pressureImgw(n),humidityImgw(n),windImgw(n));
    end
    for o=1:size(stations)(2)
      fprintf(fwd,'%s;%s;%s;%f;%f;%f;%f;%f\n',dateImgw{o},stations{o},probingTimeWtt{o},...
      tempWtt(o),feelTempWtt(o),pressureWtt(o),humidityWtt(o),windWtt(o));
    end
    fclose(fwd);
    
    fwd=fopen('weatherData.csv','r');
    dataSet=textscan(fwd,'%s %s %s %f %f %f %f %f','Delimiter',';','HeaderLines',1);
    fclose(fwd);
    for n=1:5
      [maks(n),indeksmaks(n)]=max(dataSet{n+3});
      [minim(n),indeksmin(n)]=min(dataSet{n+3});
    end     
    
    
    printf("Done!\n")
    
    printf("highest/lowest registered temperature: %.2f/%.2f [C] at %s/%s on %s/%s \n\n",maks(1),minim(1),...
    cell2mat(dataSet{3}(indeksmaks(1))),cell2mat(dataSet{3}(indeksmin(1))),cell2mat(dataSet{1}(indeksmaks(1))),...
    cell2mat(dataSet{1}(indeksmin(1))));
    
    printf("highest/lowest calculated sensed temperature: %.2f/%.2f [C] at %s/%s on %s/%s\n\n",maks(2),minim(2),...
    cell2mat(dataSet{3}(indeksmaks(2))),cell2mat(dataSet{3}(indeksmin(2))),cell2mat(dataSet{1}(indeksmaks(2))),...
    cell2mat(dataSet{1}(indeksmin(2))));
    
    printf("highest/lowest registered pressure: %.2f/%.2f [hPa] at %s/%s on %s/%s\n\n",maks(3),minim(3),...
    cell2mat(dataSet{3}(indeksmaks(3))),cell2mat(dataSet{3}(indeksmin(3))),cell2mat(dataSet{1}(indeksmaks(3))),...
    cell2mat(dataSet{1}(indeksmin(3))));
    
    printf("highest/lowest registered humidity: %.2f/%.2f [o|o] at %s/%s on %s/%s\n\n",maks(4),minim(4),...
    cell2mat(dataSet{3}(indeksmaks(4))),cell2mat(dataSet{3}(indeksmin(4))),cell2mat(dataSet{1}(indeksmaks(4))),...
    cell2mat(dataSet{1}(indeksmin(4))));
    
    printf("highest/lowest registered wind speed: %.2f/%.2f [km/h] at %s/%s on %s/%s\n\n",maks(5),minim(5),...
    cell2mat(dataSet{3}(indeksmaks(5))),cell2mat(dataSet{3}(indeksmin(5))),cell2mat(dataSet{1}(indeksmaks(5))),...
    cell2mat(dataSet{1}(indeksmin(5))));
  
    %pause to download data only one time on a right minute
    %solution by Rafal Osadnik
    pause(60)
    
    
  end
end